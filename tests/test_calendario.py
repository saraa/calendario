#!/usr/bin/python3

import contextlib
import io
import sys
import unittest
from unittest import mock
from unittest.mock import patch

from calendario import (add_activity, get_busiest, get_time,
                        get_all, show, get_activity, menu, main)
import calendario


class TestAddActivity(unittest.TestCase):

    def setUp(self):
        calendario.calendar = {}

    def test_add_activity(self):
        date = "2023-01-01"
        time = "12:00"
        activity = "Lunch"
        add_activity(date, time, activity)
        self.assertIn(date, calendario.calendar)
        self.assertIn(time, calendario.calendar[date])
        self.assertEqual(calendario.calendar[date][time], activity)

    def test_new_date(self):
        new_date = "2023-02-15"
        time = "15:30"
        activity = "Doctor's appointment"

        add_activity(new_date, time, activity)

        self.assertIn(new_date, calendario.calendar)
        self.assertIn(time, calendario.calendar[new_date])
        self.assertEqual(calendario.calendar[new_date][time], activity)

    def test_multiple_activities(self):
        date = "2023-03-01"

        add_activity(date, "09:00", "Breakfast")
        add_activity(date, "12:00", "Lunch with friends")

        self.assertIn(date, calendario.calendar)
        self.assertEqual(len(calendario.calendar[date]), 2)
        self.assertIn("09:00", calendario.calendar[date])
        self.assertIn("12:00", calendario.calendar[date])


class TestGetBusiest(unittest.TestCase):

    def setUp(self):
        calendario.calendar = {}

    def test_get_busiest(self):
        add_activity("2023-01-01", "09:00", "Breakfast")
        add_activity("2023-01-01", "12:00", "Lunch")
        add_activity("2023-01-02", "15:00", "Snacks")
        add_activity("2023-01-02", "18:00", "Dinner")
        add_activity("2023-01-02", "21:00", "Movie")

        busiest, activities = get_busiest()

        self.assertEqual(busiest, "2023-01-02")
        self.assertEqual(activities, 3)


    def test_no_activities(self):
        self.assertEqual(len(calendario.calendar), 0)

        busiest, activities = get_busiest()

        self.assertIsNone(busiest)
        self.assertEqual(activities, 0)


class TestGetTime(unittest.TestCase):

    def setUp(self):
        calendario.calendar = {}

    def test_get_time(self):
        add_activity("2023-01-01", "09:00", "Breakfast")
        add_activity("2023-01-01", "12:00", "Lunch")
        add_activity("2023-01-02", "12:00", "Snack")

        activities = get_time("12:00")

        self.assertEqual(len(activities), 2)
        self.assertEqual(activities[0][1], "12:00")
        self.assertEqual(activities[1][1], "12:00")
        self.assertEqual(activities[0][2], "Lunch")
        self.assertEqual(activities[1][2], "Snack")

    def test_get_time2(self):
        add_activity("2023-01-01", "09:00", "Breakfast")
        add_activity("2023-01-02", "09:00", "Brunch")

        activities = get_time("09:00")

        self.assertEqual(len(activities), 2)
        self.assertEqual(activities[0][1], "09:00")
        self.assertEqual(activities[1][1], "09:00")


class TestGetAll(unittest.TestCase):

    def setUp(self):
        calendario.calendar = {}

    def test_get_all(self):
        add_activity("2023-01-01", "09:00", "Breakfast")
        add_activity("2023-01-02", "12:00", "Lunch")

        all_activities = get_all()

        self.assertEqual(len(all_activities), 2)
        self.assertEqual(all_activities[0][0], "2023-01-01")
        self.assertEqual(all_activities[1][0], "2023-01-02")


    def test_get_all2(self):
        add_activity("2023-01-01", "12:00", "Lunch")
        add_activity("2023-01-02", "10:00", "Meeting")

        all_activities = get_all()

        self.assertEqual(len(all_activities), 2)
        self.assertEqual(all_activities[0][0], "2023-01-01")
        self.assertEqual(all_activities[0][1], "12:00")
        self.assertEqual(all_activities[1][0], "2023-01-02")
        self.assertEqual(all_activities[1][1], "10:00")

activities_str = """2023-01-01. 09:00: Breakfast
2023-01-01. 12:00: Lunch
2023-01-02. 12:00: Snack
"""

class TestShow(unittest.TestCase):

    def SetUp(self):
        calendario.calendar = {}

    def test_simple(self):
        calendario.calendar = {}
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            add_activity("2023-01-01", "09:00", "Breakfast")
            add_activity("2023-01-01", "12:00", "Lunch")
            add_activity("2023-01-02", "12:00", "Snack")
            show(get_all())
            output = fake_out.getvalue()

        self.assertEqual(activities_str, output)


activity_str = ('2023-11-12', '10:00', 'Lunch')

class TestGetActivity(unittest.TestCase):

    def SetUp(self):
        calendario.calendar = {}

    @patch('builtins.input', side_effect=['2023-11-12', '10:00', 'Lunch'])
    def test_get_activity(self, mock_inputs):
        activity = get_activity()

        self.assertEqual(activity, activity_str)

    @patch('builtins.input', side_effect=['2023-11-1', '2023-11-12', '10:00', 'Lunch'])
    def test_get_activity_error(self, mock_inputs):
        activity = get_activity()

        self.assertEqual(activity, activity_str)

    @patch('builtins.input', side_effect=['2023-11-1', '2023-11-12', 'Lunch', '2023-11-12', '10:00', 'Lunch'])
    def test_get_activity_error2(self, mock_inputs):
        activity = get_activity()

        self.assertEqual(activity, activity_str)

    @patch('builtins.input', side_effect=['2023-11-12', '25:00', '2023-11-12', '23:5', '2023-11-12', '10:00', 'Lunch', '2023-11-12', '10:00', 'Lunch'])
    def test_get_activity_error3(self, mock_inputs):
        activity = get_activity()

        self.assertEqual(activity, activity_str)


class TestMenu(unittest.TestCase):

    @patch('builtins.input', side_effect=['X'])
    def test_menu_returns_option(self, mock_input):
        opcion = menu()
        self.assertEqual(opcion, 'X')

    @patch('builtins.input', side_effect=['x'])
    def test_menu_returns_option_lower(self, mock_input):
        opcion = menu()
        self.assertEqual(opcion, 'X')


expected1 = """A. Introduce actividad
B. Lista todas las actividades
C. Día más ocupado
D. Lista de las actividades de cierta hora dada
X. Terminar
Opción: """

expected2 = """A. Introduce actividad
B. Lista todas las actividades
C. Día más ocupado
D. Lista de las actividades de cierta hora dada
X. Terminar
Opción: Fecha: Hora: Actividad: A. Introduce actividad
B. Lista todas las actividades
C. Día más ocupado
D. Lista de las actividades de cierta hora dada
X. Terminar
Opción: """

class TestMain(unittest.TestCase):

    def test_main_exit(self):
        sys.stdin = io.StringIO("X\n")
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main()
        output = fake_out.getvalue()
        self.assertEqual(expected1, output)

    @patch('calendario.add_activity')
    def test_main_add(self, mock_add):
        sys.stdin = io.StringIO("A\n2023-11-12\n10:00\nBreakfast\nX\n")
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main()
        mock_add.assert_called_once_with("2023-11-12", "10:00", "Breakfast")
        output = fake_out.getvalue()
        self.assertEqual(expected2, output)


if __name__ == '__main__':
    unittest.main()
